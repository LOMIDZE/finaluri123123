package com.example.sheicanisaqartvelo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class RegisterActivity : AppCompatActivity() {

    private lateinit var inputName: EditText
    private lateinit var inputLastname: EditText
    private lateinit var inputEmail: EditText
    private lateinit var inputPassword: EditText
    private lateinit var signUp: TextView

    private lateinit var mAuth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        mAuth = FirebaseAuth.getInstance()

        inputName = findViewById(R.id.inputName)
        inputLastname = findViewById(R.id.inputLastname)
        inputEmail = findViewById(R.id.inputEmail)
        inputPassword = findViewById(R.id.inputPassword)
        signUp = findViewById(R.id.signUp)

        fun signUp (TextView: TextView){

            val inputEmail = inputEmail.text.toString()
            val inputPassword = inputPassword.text.toString()

            if (inputEmail.isEmpty() || inputPassword.isEmpty()){
                Toast.makeText(this, "Empty", Toast.LENGTH_SHORT).show()
            }else {
                mAuth.createUserWithEmailAndPassword(inputEmail, inputPassword)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful){
                            startActivity(Intent(this, MainActivity::class.java))
                            finish()
                        }else {
                            Toast.makeText(this, "Error!!!", Toast.LENGTH_SHORT).show()
                        }


            fun inputName ( TextView: EditText) {
                val intent = Intent(this, ProfileActivity::class.java)
                val name = inputName.text.toString()
                intent.putExtra("key", name)
                startActivity(intent)
            }
                        fun inputLastname ( TextView: EditText){
                            val intent = Intent(this, ProfileActivity::class.java)
                            val lastname = inputLastname.text.toString()
                            intent.putExtra("key1", lastname)
                            startActivity(intent)
                        }


                    }
            }

        }



    }
}