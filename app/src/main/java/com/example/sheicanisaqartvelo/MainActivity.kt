package com.example.sheicanisaqartvelo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import java.net.PasswordAuthentication

class MainActivity : AppCompatActivity() {

    private lateinit var login: TextView
    private lateinit var resetPassword: TextView
    private lateinit var register: TextView
    private lateinit var password: EditText
    private lateinit var email: EditText

    private lateinit var mAuth: FirebaseAuth


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mAuth = FirebaseAuth.getInstance()

        if (mAuth.currentUser != null){
            startActivity(Intent(this, MainPageActivity::class.java))
        }

        setContentView(R.layout.activity_main)



        login = findViewById(R.id.logInTextView)
        register = findViewById(R.id.registerTextView)
        password = findViewById(R.id.password)
        email = findViewById(R.id.email)
        resetPassword = findViewById(R.id.resetPassword)

        fun login(){
            login.setOnClickListener(){
                val email = email.text.toString()

                val password = password.text.toString()

                if (email.isEmpty() || password.isEmpty()){
                    Toast.makeText(this, "Empty", Toast.LENGTH_SHORT).show()
                }else {
                    mAuth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener{ task ->
                            if (task.isSuccessful){
                                startActivity(Intent(this, MainPageActivity::class.java))
                            }else{
                                Toast.makeText(this, "Error!!!", Toast.LENGTH_SHORT).show()
                            }
                        }
                }
            }

        }

    }

}
