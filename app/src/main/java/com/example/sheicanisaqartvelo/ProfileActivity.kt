package com.example.sheicanisaqartvelo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth

class ProfileActivity : AppCompatActivity() {

    private lateinit var profileName: TextView
    private lateinit var profileLastName: TextView
    private lateinit var profileEmail: TextView
    private lateinit var logOut: TextView

    private lateinit var mAuth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        mAuth = FirebaseAuth.getInstance()

        profileName = findViewById(R.id.profileName)
        profileLastName = findViewById(R.id.profileLastName)
        profileEmail = findViewById(R.id.profileEmail)
        logOut = findViewById(R.id.logOut)

        fun name (TextView: EditText){

            val inputName = intent.getStringExtra("key")
            profileName.text = inputName
        }
        fun lastname (TextView: EditText){
            val lastname = intent.getStringExtra("key1")
            profileLastName.text = lastname
        }
        fun email (TextView: EditText){
            val email = intent.getStringExtra("kew2")
            profileEmail.text = email
        }
        fun logout (TextView: TextView){
            mAuth.signOut()
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }


    }
}